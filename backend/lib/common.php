<?php

/**
 * Shorthand functions
 */

function c($string)
{
    return addslashes($string);
}

function cc($string)
{
    $string = preg_replace('[a-zA-Z0-9_ -]', '', $string);
    return addslashes($string);
}

function e($string)
{
    return urlencode($string);
}

function d($string)
{
    return urldecode($string);
}

function re($string)
{
    return rawurlencode($string);
}

function rd($string)
{
    return rawurldecode($string);
}

function h($string)
{
    return htmlspecialchars($string);
}

function r($mixed)
{
    echo '<pre>'.print_r($mixed, true).'</pre>';
}

function dd($delay_seconds)
{
    if (abs($delay_seconds) < 3600) return ''.round($delay_seconds/60).' min';
    $h = floor($delay_seconds / 3600);
    $m = round($delay_seconds % 3600 / 60);
    return $h.' h '.$m.' min';
}

function now()
{
    return gmdate('Y-m-d H:i:s');
}

function local_to_utc($local_date)
{
    return gmdate('Y-m-d H:i:s', strtotime($local_date));
}

function utc_to_local($utc_date, $user = false)
{
    if ($user === false) $user = registry::get('web.user');
    return date($user->date_format, strtotime($utc_date.' UTC'));
}

/**
 * Useful functions
 */


function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}



function create_random_string($length)
{
    $characters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $lengthall = strlen($characters);
    $string = '';
    for ($i = 1; $i <= $length; $i++) {
        $string .= $characters[mt_rand(0, $lengthall - 1)];
    }
    return $string;
}

function check_password($password)
{
    $regexps = array(
        '/^.{6,}$/u',
        '/[a-zA-Z]/u',
        '/[0-9]/u',
    );

    foreach ($regexps as $regexp)
    {
        if (!preg_match($regexp, $password)) {
            return false;
        }
    }
    return true;
}

function create_captcha($expire = 600, $length = 4)
{
    $date = date('Y-m-d H:i:s', time() + $expire);
    $code = create_random_string($length);
    $sql = "INSERT INTO `captcha` (`code`, `expire`) VALUES ('$code', '$date')";
    return registry::get('db.core')->query($sql)->insert_id();
}

function check_captcha($id, $code)
{
    $db = registry::get('db.core');

    $id = (int)$id;

    $sql = "SELECT * FROM `captcha` WHERE `id`='$id'";
    $result = $db->query($sql);
    if ($obj = $result->fetch_object()) {
        $sql = "DELETE FROM `captcha` WHERE `id`='$id'";
        $db->query($sql);
        if (strtotime($obj->expire) > time() && $obj->code == $code) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function is_email($str)
{
    $regexps = array(
        '/^.{6,100}$/u',
        '/^[a-zA-Z0-9-\._]{1,50}@[a-zA-Z0-9-\.]{1,50}\.[a-zA-Z0-9]{2,}$/u',
    );

    foreach ($regexps as $regexp) {
        if (!preg_match($regexp, $str)) {
            return false;
        }
    }
    return true;
}

function looks_like_a_key($key)
{
    if (preg_match('/^([a-zA-Z0-9]{1,50})$/u', $key)) {
        return true;
    }
    return false;
}

function looks_like_a_tag($tag)
{
    if (preg_match('/^([a-zA-Z0-9_\-\.]{1,50})$/u', $tag)) {
        return true;
    }
    return false;
}

function x($str)
{
    return str_replace(
        array('&', '<', '>'),
        array('&amp;', '&lt;', '&gt;'),
        $str
    );
}

function explodeTree($array, $delimiter = '_', $baseval = false)
{
    if(!is_array($array)) return false;
    $splitRE   = '/' . preg_quote($delimiter, '/') . '/';
    $returnArr = array();
    foreach ($array as $key => $val) {
        // Get parent parts and the current leaf
        $parts  = preg_split($splitRE, $key, -1, PREG_SPLIT_NO_EMPTY);
        $leafPart = array_pop($parts);

        // Build parent structure
        // Might be slow for really deep and large structures
        $parentArr = &$returnArr;
        foreach ($parts as $part) {
            if (!isset($parentArr[$part])) {
                $parentArr[$part] = array();
            } elseif (!is_array($parentArr[$part])) {
                if ($baseval) {
                    $parentArr[$part] = array('__base_val' => $parentArr[$part]);
                } else {
                    $parentArr[$part] = array();
                }
            }
            $parentArr = &$parentArr[$part];
        }

        // Add the final part to the structure
        if (empty($parentArr[$leafPart])) {
            $parentArr[$leafPart] = $val;
        } elseif ($baseval && is_array($parentArr[$leafPart])) {
            $parentArr[$leafPart]['__base_val'] = $val;
        }
    }

    return $returnArr;
}

function plotTree($arr, $indent = 0, $mother_run = true, $show_delete = true){
    if ($mother_run) {
        echo '<ul class="tree root">';
    } else {
        echo '<ul class="tree">';
    }

    foreach ($arr as $k=>$v){
        // skip the baseval thingy. Not a real node.
        if ($k == "__base_val") continue;
        // determine the real value of this node.
        $show_val = (is_array($v) ? $v["__base_val"] : $v);
        // show the indents
        echo str_repeat("  ", $indent);
        if ($indent == 0) {
            // this is a root node. no parents
            //echo "O";
            if (is_array($v)){
                echo '<li class="clearfix" data-id="'.$show_val.'">
                    <span class="fa fa-caret-down child child-indicator cursor-pointer"></span>
                    <div class="channel display_inline_block">
                        <div class="pull-left">'.$k.'</div>'
                        /*. " (" . $show_val. ")" */
                        .($v["__base_val"] && $show_delete ? '<div class="pull-right"><a class="delete-channel btn btn-default" data-channel=""><span class="fa fa-trash-o"></span></a></div>' : '')
                        .'
                    </div>';

                    plotTree($v, ($indent+1), false, $show_delete);
                echo '
                </li>';
            } else {
                echo '<li class="clearfix" data-id="'.$show_val.'">
                <span class="child-indicator"></span>
                    <div class="channel display_inline_block">
                    <div class="pull-left">'.$k.'</div>'
                    /*. " (" . $show_val. ")" */
                    .'<div class="pull-right">';
                    if ($show_delete) {
                        echo '<a class="delete-channel btn btn-default" data-channel="">
                            <span class="fa fa-trash-o"></span>
                        </a>';
                    }
                    echo '
                    </div>
                </li>';
            }
        } elseif (is_array($v)){
            // this is a normal node. parents and children
            //echo "+ ";
            echo '<li class="clearfix" data-id="'.$show_val.'">
                <span class="fa fa-caret-down child child-indicator cursor-pointer"></span>
                <div class="channel display_inline_block">
                    <div class="pull-left">'.$k.'</div>'
                    /*. " (" . $show_val. ")" */
                    .($v["__base_val"] && $show_delete ? '<div class="pull-right"><a class="delete-channel btn btn-default" data-channel=""><span class="fa fa-trash-o"></span></a></div>' : '')
                    .'
                </div>';

                plotTree($v, ($indent+1), false, $show_delete);
            echo '
            </li>';
        } else {
            // this is a leaf node. no children
            //echo "- ";
            echo '<li class="clearfix" data-id="'.$show_val.'">
                <span class="child-indicator"></span>
                <div class="channel display_inline_block">
                    <div class="pull-left">'.$k.'</div>'
                    /*. " (" . $show_val. ")" */
                    .'<div class="pull-right">';
                        if ($show_delete) {
                            echo '<a class="delete-channel btn btn-default" data-channel="" >
                                    <span class="fa fa-trash-o"></span>
                                </a>';
                        }
                        echo '
                    </div>
                </div>
            </li>';
        }
    }

    if ($mother_run) {
        echo '</ul>';
    } else {
        echo '</ul>';
    }
}

/**
 * Filter var
 */
function filter_array_level_2($arr, $allowed)
{
    foreach ($arr as $k1 => $v1) {
        foreach ($v1 as $k2 => $v2) {
            if (!in_array($k2, $allowed)) {
                unset($arr[$k1][$k2]);
            }
        }
    }
    return $arr;
}

function clean_null_recursive($array, $nullify = false)
{

    foreach ($array as $k => $v) {
        if (is_array($v)) {
            $array[$k] = clean_null_recursive($v, true);
        }
        if ($array[$k] === null) {
            unset($array[$k]);
        }
    }
    if (is_array($array) && count($array) == 0 && $nullify === true) {
        return null;
    }
    return $array;
}

function array_load_recursive($a, $b)
{
    $array = $a;
    foreach ($array as $k => $v) {
        if (is_array($v)) {
            $array[$k] = array_load_recursive($v, isset($b[$k]) ? $b[$k] : []);
        } elseif (isset($b[$k])) {
            $array[$k] = $b[$k];
        }
    }
    return $array;
}
