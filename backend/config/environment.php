<?php
/**
 * THIS FILE IS NOT VERSION CONTROLLED
 */

/**
 * Error Reporting
 */

error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);

/**
 * Environment
 */

define('ENVIRONMENT', 'development');

$development_ips = array(
    '10.0.0.1',
    '10.0.0.235',
    '10.0.0.236',
);

if (in_array($_SERVER['REMOTE_ADDR'], $development_ips)) {
    define('MAINTENANCE', false);
} else {
    define('MAINTENANCE', true);
}

define('PRODUCTION_SERVER', 'adaplo');
