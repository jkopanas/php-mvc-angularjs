<?php

if (!defined('JSON_UNESCAPED_UNICODE'))
{
	define('JSON_UNESCAPED_UNICODE', 256);
}

if (!defined('JSON_PRETTY_PRINT'))
{
	define('JSON_PRETTY_PRINT', 128);
}

/**
 * PHP Configuration
 */

mb_internal_encoding('UTF-8');

/**
 * APPLICATION IMAGES
 */
define('APPLICATION_SALT', '2YXDulEiHknbdSPefkSH');
define('APPLICATION_IMAGE_LARGE', '300');
define('APPLICATION_IMAGE_SMALL', '100');

/**
 * DEFAULT VALUES
 */
define('DEFAULT_PER_PAGE', 20);











