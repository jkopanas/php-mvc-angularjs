<?php
define('ROOT', dirname(__DIR__));

require 'init/web.php';

/**
 * Utility functions
 */


function l($url)
{
	return PATH.$url;
}

function fl($url = '')
{
	return FULLPATH.$url;
}

/**
 * Run
 */

function run()
{

	$url = '/'.$_GET['url'];

	$url_array = explode('/', $url);
	array_shift($url_array);
	//r($url_array);

	$controller = !empty($url_array[0]) ? $url_array[0] : 'index';
	$controller = str_replace('-', '_', $controller);
	$controller_name = 'Adaplo\\Controllers\\'.ucfirst($controller);
	array_shift($url_array);

	$action = !empty($url_array[0]) ? $url_array[0] : 'index';
	$action = str_replace('-', '_', $action);
	$action_name = $action.'_action';
	array_shift($url_array);

	$controller_object = new $controller_name();
	$controller_object->set_action($action);
	$controller_object->set_parameters($url_array);
	$controller_object->run();
}

/**
 * Execute
 */

run();


