<?php
defined('ROOT') or die();


require 'config/environment.php';

/**
 * Define contants
 */
if ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'adaplo' )
{

		define('USERDIR', null);
		define('PATH', 'client');
		define('FULLPATH', 'https://'.$_SERVER['HTTP_HOST'].'/'.PRODUCTION_SERVER.'/backend');
		define('SITE', 'Adaplo');
}
elseif (in_array($_SERVER['HTTP_HOST'], array(PRODUCTION_SERVER)))
{
	define('USERDIR', null);
	define('PATH', 'client');
	define('FULLPATH', 'https://'.$_SERVER['HTTP_HOST'].'/'.PRODUCTION_SERVER.'/backend');
	define('SITE', 'Adaplo');
}
else
{
	die('cannot work here');
}

define('DOCSPATH', PATH);

/**
 * Configuration files
 */

require 'config/base.php';

/**
 * Libraries
 */
require 'lib/common.php';

/**
 * Classes
 */
require_once '../vendor/campaignmonitor/createsend-php/csrest_general.php';
require_once '../vendor/campaignmonitor/createsend-php/csrest_clients.php';
require_once '../vendor/campaignmonitor/createsend-php/csrest_subscribers.php';
require_once '../vendor/campaignmonitor/createsend-php/csrest_lists.php';
require '../vendor/Adaplo/Autoloader/Autoloader.php';
$autoloader = new Adaplo\Autoloader\Autoloader();
spl_autoload_register(array($autoloader, 'autoload'));
//spl_autoload_register(array($autoloader, 'legacy'));

/**
 * BUILD REGISTRY
 */


Adaplo\Registry\Registry::set('web.session_name', 'adaplo');
Adaplo\Registry\Registry::set('web.site_name', 'Adaplo');

Adaplo\Registry\Registry::set('config.email_from_address', 'gkopanas@hotmail.com');
