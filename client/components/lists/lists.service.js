'use strict';

angular.module('Adaplo')
    .factory('Lists', function Lists($location, $rootScope, $http,Account, $cookieStore, $cookies, $q, $state) {

        return {
            getLists: function (user, callback) {
                var cb = callback || angular.noop;
                var deferred = $q.defer();

                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                $http({
                        'url': '/backend/lists/index',
                        'method': 'GET',
                    })
                    .then( function (data) {
                        if (!data.error) {
                                return cb(data);
                        }
                    }, function (err) {
                        deferred.reject(err);
                        return cb(err);
                    });

                return deferred.promise;
            },
            getList: function (id, callback) {
                var cb = callback || angular.noop;
                var deferred = $q.defer();

                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                $http({
                    'url': '/backend/lists/list/'+id,
                    'method': 'GET',
                })
                    .then( function (data) {
                        if (!data.error) {

                            return cb(data);
                        }
                    }, function (err) {
                        deferred.reject(err);
                        return cb(err);
                    });

                return deferred.promise;
            },
            addToList: function (data, callback) {
                var cb = callback || angular.noop;
                var deferred = $q.defer();

                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                $http({
                    'url': '/backend/subscribers/add/',
                    'method': 'POST',
                    'data': $.param(data)
                })
                    .then( function (data) {
                        if (!data.error) {
                            return cb(data);
                        }
                    }, function (err) {
                        deferred.reject(err);
                        return cb(err);
                    });

                return deferred.promise;
            },
            removeFromList: function (data, callback) {
                var cb = callback || angular.noop;
                var deferred = $q.defer();

                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                $http({
                    'url': '/backend/subscribers/remove/',
                    'method': 'POST',
                    'data': $.param(data)
                })
                    .then( function (data) {
                            return cb(data);
                    }, function (err) {
                        deferred.reject(err);
                        return cb(err);
                    });

                return deferred.promise;
            }
        };
    });
