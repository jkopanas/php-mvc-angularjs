'use strict';

angular.module('Adaplo')
    .factory('Auth', function Auth($location, $rootScope, $http,Account, $cookieStore, $cookies, $q, $state) {

        var currentUser = {};

        if ($cookies.get('token')) {
            currentUser = Account.get();

        }

        return {

            /**
             * Authenticate user and save ClientID
             *
             * @param  {Object}   user     - login info
             * @param  {Function} callback - optional
             * @return {Promise}
             */
            login: function (user, rememberMe, callback) {
                var cb = callback || angular.noop;
                var deferred = $q.defer();

                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

                $http({
                        'url': '/backend/auth/login',
                        'method': 'POST',
                        data:$.param({
                            apikey: user.apikey
                        })
                    })
                    .then( function (data) {
                        if (data.login != "failed") {

                            $cookies.put('token', data.data.data.ClientID);

                            currentUser = Account.get( function (userData) {

                                $state.go('main.admin.lists');

                                deferred.resolve(userData);
                                return cb();
                            });

                        } else {
                            $state.go('home.login');
                        }
                    }, function (err) {
                        deferred.reject(err);
                        return cb(err);
                    });

                return deferred.promise;
            },

            /**
             * Gets all available info on authenticated user
             *
             * @return {Object} user
             */
            getCurrentUser: function () {
                return currentUser;
            },

            /**
             * Check if a user is logged in
             *
             * @return {Boolean}
             */
            isLoggedIn: function() {
                if (currentUser.user) {
                    return currentUser.user;
                }
                return false;
            },

            /**
             * Waits for currentUser to resolve before
             * checking if user is logged in
             */
            isLoggedInAsync: function(cb) {

                if (currentUser.hasOwnProperty('$promise')) {

                    currentUser
                        .$promise
                        .then(  function() { cb(true)  ; })
                        .catch( function() { cb(false) ; });

                }

                else if (currentUser.hasOwnProperty('role')) {
                    cb(true);
                }

                else {
                    cb(false);
                }

            },

            /**
             * Get auth token
             */
            getToken: function() {
                return $cookies.get('token');
            }
        };
    });
