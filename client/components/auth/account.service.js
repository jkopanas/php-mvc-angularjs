'use strict';

angular.module('Adaplo')
    .factory('Account', function ($resource) {

        return $resource('/backend/account/:id/:controller',

            { id: '@_id' },

            {
                get: {
                    method: 'GET',
                    params: {
                        id: 'me'
                    }
                },

                resetPassword: {
                    method: 'PUT',
                    params: {
                        id: 'reset'
                    }
                },

                update: {
                    method: 'PUT'
                }

            }

        );
    });
