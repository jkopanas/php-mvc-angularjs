'use strict';

angular.module('Adaplo')
    .controller('NavbarCtrl', function ($scope, $state, $stateParams, Auth, Lists) {

        var self = this;

        Auth.getCurrentUser().$promise.then( function (data) {
            if (data.user) {
                var user = data.user;
                self.name = user.ClientID;
                self.isLoggedIn = Auth.isLoggedIn;
                self.isAdmin = Auth.isAdmin;
            }
        });




    });
