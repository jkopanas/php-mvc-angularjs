'use strict';

angular.module('Adaplo', [
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'smart-table'
    ])
    .config( function  ($stateProvider     ,
                        $urlRouterProvider ,
                        $locationProvider  ,
                        $httpProvider      ,
                        $compileProvider    ) {

        $stateProvider
            .state('main', {

                abstract : true,
                views    : {

                    'mainMenu': {
                        templateUrl  : 'client/components/navbar/navbar.html',
                        controller   : 'NavbarCtrl',
                        controllerAs : 'navbar'
                    },

                    '': {
                        templateUrl  : 'client/app/main/main.html',
                        controller   : 'MainCtrl',
                        controllerAs : 'main'
                    }

                }

            });

        //$locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('authInterceptor');

        $urlRouterProvider.otherwise('/');
    })

    .factory('authInterceptor', function ($q, $cookies, $location) {
        return {

            // Add authorization token to headers
            request: function(config) {

                config.headers = config.headers || {};
                var cookie = $cookies.get('token');

//                if (cookie) {
                //    config.headers.Authorization = 'Bearer ' + cookie;
                //}

                return config;
            },

            // Intercept 401s and redirect you to login
            responseError: function(response) {

                if (response.status === 401) {

                    $location.path('/login');
                    $cookies.remove('token');
                }

                return $q.reject(response);

            }
        };
    })
    .run(function ($rootScope, $state, $location, Auth) {

        $rootScope.stateIsLoading = false;

        $rootScope.$on('$stateChangeStart', function (event, next) {


            if (next.authenticate) {
                Auth.isLoggedInAsync( function (loggedIn) {
                    if ( !loggedIn ) {
                        event.preventDefault();
                        $state.go('home.login');
                    }
                });
            }

            $rootScope.pageTitle = next.title || ''; // <=== Added page title

        });

        $rootScope.$on('$stateChangeSuccess', function() {
            $rootScope.stateIsLoading = false;
        });

    });
