'use strict';

angular.module('Adaplo')
    .config(function ($stateProvider,$urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: "/",
                views: {
                    "mainMenu@home": {
                        templateUrl: 'client/app/home/mainmenu/mainMenu.html',
                        controller:'MainMenuCtrl'
                    },
                    "": {
                        templateUrl: 'client/app/home/home.html',
                        controller:'HomeCtrl'
                    }
                }
            });
        $urlRouterProvider.otherwise("/");
    });
