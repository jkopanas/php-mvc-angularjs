'use strict';

angular.module('Adaplo')

    .controller('LoginCtrl', function ($scope, Auth, $location, $window) {

        $scope.usersignin = false;
        $scope.signin = function (apikey) {

                $scope.error = false;
                $scope.usersignin = true;

                Auth
                    .login({
                        apikey: apikey
                    }, 0)
                    .then( function () {
                        $scope.usersignin = false;
                    })
                    .catch( function (err) {

                        console.log(err);

                        if (err.message === 'The client has been deactivated.') {
                            $scope.error = true;
                            $scope.errorMessage = 'The client has been deactivated.';
                        }

                        else if (err.message === 'The client account has expired.') {
                            $scope.error = true;
                            $scope.errorMessage = 'The client account has expired.';
                        }

                        else if (err.message === 'This email is not registered.') {
                            $scope.error = true;
                            $scope.errorMessage = 'This email is not registered.';
                        }

                        else if (err.message === 'This password is not correct.') {
                            $scope.error = true;
                            $scope.errorMessage = 'This password is not correct.';
                        }

                        else if (err.message === 'This email is not verified.') {
                            $scope.error = true;
                            $scope.errorMessage = 'This email is not verified.';
                        }

                        $scope.usersignin = false;
                    });

        };

    });
