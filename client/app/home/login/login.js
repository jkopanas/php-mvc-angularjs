'use strict';

angular.module('Adaplo')
    .config( function ($stateProvider) {

        $stateProvider
            .state('home.login', {
                url         : 'login',
                templateUrl : 'client/app/home/login/login.html',
                controller  : 'LoginCtrl'
            });

    });
