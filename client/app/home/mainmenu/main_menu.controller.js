'use strict';
angular.module('Adaplo')
    .controller('MainMenuCtrl', function ($scope) {

        $scope.moveLeft = function () {


            angular.element('#textbox').animate({
                'marginLeft' : "0"
            });

            angular.element('.toplam').animate({
                'marginLeft' : "100%"
            });

        };

        $scope.moveRight = function () {

            var element =  document.getElementById('toplam');
            var marginLeft = document.defaultView.getComputedStyle(element, "").getPropertyValue('margin-left').charAt(0);

            if(marginLeft !== '0' ) {
                angular.element('#textbox').animate({
                    'marginLeft': "50%"
                });

                angular.element('.toplam').animate({
                    'marginLeft': "0"
                });
            }
        };

    });