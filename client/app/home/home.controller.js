'use strict';
angular.module('Adaplo')
    .controller('HomeCtrl', function ($scope, $state, Auth) {

        // redirect if user is in an authenticated state
        Auth.isLoggedInAsync(
            function (loggedIn) {
                if (loggedIn) {
                    $state.go('main.admin.lists');
                } else {
                    $state.go('home.login');
                }
            }

        );

    });
