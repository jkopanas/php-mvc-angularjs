'use strict';
angular.module('Adaplo')
    .config(function ($stateProvider) {
        $stateProvider

            .state('main.admin.lists', {
                parent: 'main.admin',
                url: '/lists',
                title: 'Administrator Dashboard -',
                authenticate: true,
                views: {
                    '': {
                        templateUrl: 'client/app/admin/lists/lists.html',
                        controller: 'AdminCtrl',
                        controllerAs: 'admin'
                    },
                    'footer@main.admin.users': {
                        templateUrl: 'client/components/footer/footer_new.html'
                    }
                }

            })
            .state('main.admin.list', {
                parent: 'main.admin',
                url: '/lists/:listid',
                title: 'Administrator Dashboard -',
                authenticate: true,
                views: {
                    '': {
                        templateUrl: 'client/app/admin/lists/list.html',
                        controller: 'ListCtrl',
                        controllerAs: 'list'
                    },
                    'footer@main.admin.users': {
                        templateUrl: 'client/components/footer/footer_new.html'
                    }
                }

            })
            .state('main.admin.list.add', {
                parent: 'main.admin',
                url: '/lists/add/:listid',
                title: 'Administrator Dashboard -',
                authenticate: true,
                views: {
                    '': {
                        templateUrl: 'client/app/admin/lists/add.html',
                        controller: 'ListAddCtrl',
                        controllerAs: 'listadd'
                    },
                    'footer@main.admin.users': {
                        templateUrl: 'client/components/footer/footer_new.html'
                    }
                }
            });

    });
