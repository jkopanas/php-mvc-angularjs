'use strict';

angular.module('Adaplo')
    .config(function ($stateProvider) {
        $stateProvider
            .state('main.admin', {
                abstract : true,
                parent   : 'main',
                url      : '/admin',
                views    : {
                    '': { templateUrl: 'client/app/admin/admin.html' }
                },
            });
    });
