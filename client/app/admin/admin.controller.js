 'use strict';

 angular.module('Adaplo')
   .controller('AdminCtrl', function ($http, Lists, Auth) {
        var self = this;

        Lists.getLists(Auth.getCurrentUser(),function(data){
            self.lists = data.data.lists;
        });

     })
     .controller('ListCtrl', function ($http, $scope, Lists, $stateParams) {
         var self = this;

         var id = $stateParams.listid;
         Lists.getList(id,function(data){
             self.list = data.data.list;
             self.stat = data.data.stat;
             self.subscribers = data.data.subscribers;
         });


         $scope.removeFromList = function (email) {

             var data = {
                 id: id,
                 email: email
             };
            Lists.removeFromList(data,function(data){

                for (var i=0; i<self.subscribers.length; i++) {
                    if (self.subscribers[i].EmailAddress == data.data.unsubscriber) {
                        self.subscribers.splice(i, 1);
                        break;
                    }
                }
            })
         }
     })
     .controller('ListAddCtrl', function ($http, $scope, $state, Lists, $stateParams) {
         var self = this;

         var id = $stateParams.listid;
        $scope.listid = id;

         $scope.addtolist = function () {

             var data = {
                 "id": id,
                 "email": $scope.email,
                 "name": $scope.name
             };
             Lists.addToList(data,function(data){

                 $state.go('main.admin.list',{"listid": id})
             });
         }

     });

