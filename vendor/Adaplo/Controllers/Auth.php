<?php
namespace Adaplo\Controllers;
use Adaplo\Classes\Controller;
use Adaplo\Classes\session;
class Auth extends Controller
{
    public $controller = 'auth';

    public function before_action()
    {
        //parent::before_action();
    }


    /**
     * Login
     */

    public function login_action()
    {

        $view = $this->init_view();

        if ($this->post_valid()) {
            $auth = array('api_key' => $_POST['apikey']);
            $wrap = new \CS_REST_General($auth);
            $result = $wrap->get_clients();
            $data = $result->response;
            if (is_array($data)) {
                if (isset($data[0])) {

                    $dat = array_merge((array)$data[0],['apikey' => $_POST['apikey']]);
                    session::set('web.user', $dat);
                    $view->set('data', [
                        'login' => 'success',
                        'data' => $data[0]
                    ]);
                    $view->render('json');
                    return;
                }
            }
        }
        $view->set('data', ['login' => 'failed']);
        $view->render('json');
    }

}






