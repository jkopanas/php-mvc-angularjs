<?php
namespace Adaplo\Controllers;
use Adaplo\Classes\Controller;

class Lists extends Controller
{
	public $controller = 'lists';

	public function before_action()
	{
		//$this->add_restricted_role('anonymous');
		//parent::before_action();
	}



	public function index_action()
	{
		$view = $this->init_view();
		$view->add_title('Lists');

		$auth = array('api_key' => $this->user['apikey']);

		$wrap = new \CS_REST_Clients($this->user['ClientID'], $auth);

		$result = $wrap->get_lists();

		if($result->was_successful()) {
			$view->set('data', ['lists' => $result->response]);
		} else {
			$view->set('data', ['error' => $result->response]);
		}
		$view->render('json');
	}

	public function list_action($id)
	{
		$view = $this->init_view();


		$view->add_title('List');

		$auth = array('api_key' => $this->user['apikey']);


		$wrap = new \CS_REST_Lists($id, $auth);

		$result = $wrap->get();
		$stats = $wrap->get_stats();

		$subscribers = $wrap->get_active_subscribers(date('Y-m-d', strtotime('-30 days')), 1, 50, 'email', 'asc');

		if($result->was_successful()) {
			$view->set('data', [
				'list' => $result->response,
				'stat' => $stats->response,
				'subscribers' => $subscribers->response->Results
			]);
		} else {
			$view->set('data', ['error' => $result->response]);
		}
		$view->render('json');
	}

    
}






