<?php
namespace Adaplo\Controllers;
use Adaplo\Classes\Controller;

class Account extends Controller
{
	public $controller = 'account';

	public function before_action()
	{
		//$this->add_restricted_role('anonymous');
		//parent::before_action();
	}

	/**
	 * Login
	 */

	public function me_action()
	{
		$view = $this->init_view();
		$view->add_title('Account');

		$auth = array('api_key' => $this->user['apikey']);

		$wrap = new \CS_REST_Clients($this->user['ClientID'], $auth);
		$result = $wrap->get();
        $view->set('data', ['user' => $result->response->BasicDetails]);
		$view->set('selected_menu', 'account');
		$view->render('json');
	}


}






