<?php
namespace Adaplo\Controllers;
use Adaplo\Classes\Controller;

class Subscribers extends Controller
{
	public $controller = 'subscribers';

	public function before_action()
	{
		//$this->add_restricted_role('anonymous');
		//parent::before_action();
	}


	public function add_action()
	{
		$view = $this->init_view();
		$view->add_title('Subscribers');

		$auth = array('api_key' => $this->user['apikey']);

		$wrap = new \CS_REST_Subscribers($_POST['id'], $auth);
		$result = $wrap->add(array(
			'EmailAddress' => $_POST['email'],
			'Name' => $_POST['name'],
			'Resubscribe' => true
		));

		if($result->was_successful()) {
			$view->set('data', ['subscriber' => $result->response]);
		} else {
			$view->set('data', ['error' => $result->response]);
		}
		$view->render('json');
	}


	public function remove_action()
	{
		$view = $this->init_view();
		$view->add_title('Subscribers');

		$auth = array('api_key' => $this->user['apikey']);

		$wrap = new \CS_REST_Subscribers($_POST['id'], $auth);
		$result = $wrap->delete($_POST['email']);
		if($result->was_successful()) {
			$view->set('data', ['unsubscriber' => $_POST['email']]);
		} else {
			$view->set('data', ['error' => $result->response]);
		}
		$view->render('json');
	}

    
}






