<?php

namespace Adaplo\Autoloader;

/**
 * Autoloader
 */

class Autoloader
{
	/**
	 * Constructor
	 */
	public function __construct()
	{

	}

	/**
	 * Autoload
	 */
	public function autoload($class_name)
	{
		$class_name = ltrim($class_name, '\\');
		$file_name = '';
		$namespace = '';
	    if ($last_ns_pos = strrpos($class_name, '\\')) {
	        $namespace = substr($class_name, 0, $last_ns_pos);
	        $class_name = substr($class_name, $last_ns_pos + 1);
	        $file_name = str_replace('\\', DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR;
	    }

	    $file_name .= str_replace('_', DIRECTORY_SEPARATOR, $class_name).'.php';

	    $possible = array(
	    	'../vendor/'.$file_name,
	    );

	    foreach ($possible as $file) {
	    	if (file_exists($file)) {
	    		require $file;
	    		return;
	    	}
	    }
    }

    /**
     * Extra Autoload
     */
    public function legacy($class_name)
    {
    	$locations = array(
    		'backend/app/classes/'.strtolower($class_name).'.php',
    		'backend/app/controllers/'.strtolower($class_name).'.php',
    	);

    	foreach ($locations as $location) {
    		if (file_exists($location)) {
    			require_once $location;
    			return true;
    		}
    	}
    }

}






