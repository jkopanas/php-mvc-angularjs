<?php
namespace Adaplo\Classes;
use Adaplo\Registry\Registry as registry;

class Controller
{
	public $controller;
	public $controller_name;

	public $required_roles = array();
	public $restricted_roles = array();

	public $subaction;

	public function __construct()
	{
		register_shutdown_function(array($this, 'shutdown_function'));

		$this->sanitize_all();
		session::init(registry::get('web.session_name'));
		session::load();

		$session_user = session::get('web.user');

        if (empty($session_user) && !is_a($this,"Adaplo\Controllers\Auth") ) {

                $this->redirect('/client/auth/login');
        }



		$this->user = $session_user;
	}

	/**
	 * Basic functions
	 */

	public function set_action($action)
	{
		$this->action = $action;
		$this->action_name = $action.'_action';
	}

	public function set_parameters($parameters)
	{
		$this->parameters = $parameters;
	}

	public function set_subaction($subaction)
	{
		$this->subaction = $subaction;
		$this->subaction_name = $subaction.'_action';
	}

	public function run()
	{
		$this->before_action($this->parameters);

		if (method_exists($this, $this->action_name)) {
			call_user_func_array(array($this, $this->action_name), $this->parameters);
		} else {
            error('No such action '.$this->action_name);
            header('Location: '.l('/error/404'));
            die();
		}
		$this->after_action($this->parameters);
	}

	public function before_action()
	{
		$this->authorize();
	}

	public function after_action()
	{
		session::save();
	}


	/**
	 * Redirects
	 */
	public function redirect($url)
	{
		session::save();
		header('Location:'.$url);
		die();
	}

	public function go_to($url)
    {
		session::save();
        echo PATH;
        exit;
		header('Location:'.PATH.$url);
		die();
	}

	public function go_to_referrer()
	{
		if (!empty($_SERVER['HTTP_REFERER']))
		{
			$this->redirect($_SERVER['HTTP_REFERER']);
		}
	}

	/**
	 * View
	 */

	protected function init_view()
	{
		$view = new View($this->controller, $this->action);
		$view->set_subaction($this->subaction);
		$view->parameters = $this->parameters;

		return $view;
	}

	/**
	 * Post Validity
	 */

	protected function post_valid($security = false)
	{
		if ($security == false)
		{
			if (count($_POST) > 0)
			{
				return true;
			}
		}
		elseif ($security == 'captcha')
		{
			if (count($_POST) > 0 && check_captcha($_POST['captcha_id'], $_POST['captcha']))
			{
				return true;
			}
		}

		return false;

	}

	/**
	 * Authorization
	 */

	public function authorize()
	{
		$USER = registry::get('web.user');

		foreach ($this->required_roles as $k => $v)
		{
			if (!$USER->has_role($v))
			{
				if ($USER->has_role('anonymous')) {
				    $this->go_to('/login');
                } else {
                    error('User '.$USER->id.' not authorized for role '.$v);
                    $this->go_to('/error/restricted');
                }
			}
		}
	}

	/**
	 * Role management
	 */

	public function add_required_role($role)
	{
		$this->required_roles[] = $role;
	}

	public function add_required_roles($roles)
	{
		$this->required_roles = array_merge($this->required_roles, $roles);
	}

	public function remove_required_role($role)
	{
		$this->required_roles = array_diff($this->required_roles, array($roles));
	}

	public function remove_required_roles($role)
	{
		$this->required_roles = array_diff($this->required_roles, $roles);
	}

	public function add_restricted_role($role)
	{
		$this->restricted_roles[] = $role;
	}

	public function add_restricted_roles($roles)
	{
		$this->restricted_roles = array_merge($this->required_roles, $roles);
	}

	public function remove_restricted_role($role)
	{
		$this->restricted_roles = array_diff($this->restricted_roles, array($roles));
	}

	public function remove_restricted_roles($role)
	{
		$this->restricted_roles = array_diff($this->restricted_roles, $roles);
	}

	/**
	 * Cookie management
	 */

	public function set_cookie($name, $value, $expire_minutes)
	{
		return setcookie($name, $value, time()+$expire_minutes*60, '/');
	}

	public function unset_cookie($name)
	{
		return setcookie($name, '', time()-1);
	}

	public function show_cookies()
	{
		r($_COOKIE);
	}

	/**
	 * Sanitization
	 */

	public function sanitize_all()
	{
		$_GET    = $this->sanitize($_GET);
		$_POST   = $this->sanitize($_POST);
		$_COOKIE = $this->sanitize($_COOKIE);
	}

	public function sanitize($var)
	{
		if (is_array($var) || is_object($var))
		{
			foreach ($var as $k => $v)
			{
				$var[$k] = $this->sanitize($v);
			}
		}
		elseif (is_string($var))
		{
			$var = str_replace(array("\r\n", "\r"), "\n", $var);
		}

		return $var;
	}

	/**
	 * Shutdown
	 */

	public function shutdown_function()
	{



	}

}




