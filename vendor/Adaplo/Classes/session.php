<?php
namespace Adaplo\Classes;

class session
{
	private static $items = array();

	/**
	 * Core
	 */

	public static function init($name)
	{
		session_name($name);
		session_start();
	}

	public static function destroy()
	{
		session_unset();
		session_destroy();
		self::$items = array();
	}

	public static function load()
	{
		self::$items = $_SESSION['items'];
	}

	public static function save()
	{
		$_SESSION['items'] = self::$items;
	}

	public static function get($name)
	{
		return self::$items[$name];
	}

	public static function set($name, $value)
	{
		self::$items[$name] = $value;
	}

	public static function remove($name)
	{
		unset(self::$items[$name]);
	}





}




