<?php
namespace Adaplo\Classes;

use Adaplo\Registry\Registry as registry;

class View
{
	protected $controller;
	protected $action;
	protected $variables = array();

	public function __construct($controller, $action)
	{
		$this->controller = $controller;
		$this->controller_name = $controller.'_controller';
		$this->action = $action;
		$this->action_name = $action.'_action';

		$this->lang = registry::get('web.locale');

		$this->add_set('xhtml_title_array', registry::get('web.site_name'));
	}

	public function set_subaction($subaction)
	{
		$this->subaction = $subaction;
	}

	/**
	 * Set
	 */
	function set($name, $value)
	{
		$this->variables[$name] = $value;
	}

	public function add_set($name, $value)
	{
		$this->variables[$name][] = $value;
	}

	public function parse_set($name, $file)
	{
		extract($this->variables);
		ob_start();
		require 'backend/app'.$file;
		$this->set($name, ob_get_clean());
	}

	public function add_parse_set($name, $file)
	{
		extract($this->variables);
		ob_start();
		require 'backend/app'.$file;
		$this->add_set($name, ob_get_clean());
	}

	public function add_title($string)
	{
		$this->add_set('xhtml_title_array', $string);
	}

	/**
	 * Display view
	 */
    function render($layout = 'back')
	{
		$this->$layout();
    }

    public function ajax()
    {
		$inc = empty($this->subaction) ? $this->action : $this->subaction;
		if (file_exists('app/views/'.$this->controller.'/'.$this->lang.'/'.$inc.'.php'))
		{
			$this->parse_set('data', '/views/'.$this->controller.'/'.$this->lang.'/'.$inc.'.php');
		}
		else
		{
			$this->parse_set('data', '/views/'.$this->controller.'/'.$inc.'.php');
		}
		extract($this->variables);
		require 'app/layouts/ajax.php';
    }

    public function json()
    {
		extract($this->variables);
		require 'app/layouts/json.php';
    }
}
